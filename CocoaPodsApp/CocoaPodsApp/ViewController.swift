//
//  ViewController.swift
//  CocoaPodsApp
//
//  Created by PAOLA GUAMANI on 13/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var pokeTable: UITableView!
    var selectRowPoke = 0
    
    var pokemon:[Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let network = Networking()
        network.getAllPokemon{ (pokemonArray) in
            self.pokemon = pokemonArray
            //cada q llega, actualizo mi tabla
            self.pokeTable.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInfoPoke"{
            let destination = segue.destination as! InfoPokeViewController
            let selectedRow = pokeTable.indexPathsForSelectedRows![0]
            destination.pokemonInfo = pokemon[selectRowPoke]
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectRowPoke = indexPath.row
        performSegue(withIdentifier: "toInfoPoke", sender: self)
    }
    
    
}

