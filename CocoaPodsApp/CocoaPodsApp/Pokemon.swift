//
//  Pokemon.swift
//  CocoaPodsApp
//
//  Created by PAOLA GUAMANI on 13/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
}

struct Sprite: Decodable {
    var defaultSprite: String
    
    enum CodingKeys: String, CodingKey{
        case defaultSprite = "front_default"
    }
}
