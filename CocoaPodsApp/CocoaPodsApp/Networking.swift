//
//  Networking.swift
//  CocoaPodsApp
//
//  Created by PAOLA GUAMANI on 13/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Networking {
    
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray: [Pokemon] = []
        //nuevo grupo q ayudara a manejar cosas syncronas
        let group = DispatchGroup()
        for i in 1...8{
            group.enter()
            //le digo que lo que viene entra en el grupo
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                print(pokemon)
                pokemonArray.append(pokemon)
                group.leave()
                //entra 8 veces y sale 8 veces
            }
        }
        //group.notify espera a que el grupo este vacio
        //.main se va al hilo principal
        group.notify(queue: .main){
            completion(pokemonArray)
        }
    }
    
    func getPokemonImage(_ id:Int, completionHandler: @escaping(UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        /*Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }*/
        
    }
    
    
}
