//
//  InfoPokeViewController.swift
//  CocoaPodsApp
//
//  Created by PAOLA GUAMANI on 20/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class InfoPokeViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var imagenPokemonView: UIImageView!
    
    var pokemonInfo:Pokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = String(pokemonInfo.name)
        weightLabel.text = String(pokemonInfo.weight)
        heightLabel.text = String(pokemonInfo.height)
        
        get_Img((pokemonInfo.sprites.defaultSprite), imagenPokemonView)
        /*if pokemonInfo?.pokemon.sprites == nil {
            
            let bm = Networking()
            
            bm.getPokemonImage((pokemonInfo?.index)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.imagenPokemonView.image = imageR
                }
                
            })
            
        } else {
            
            //imagenPokemonView.image = pokemonInfo?.pokemon.sprites
            
        }*/
    }

    
    func get_Img(_ url_str:String,_ imageView:UIImageView){
        let url:URL = URL(string: url_str)!
        let session = URLSession.shared
        let task = session.dataTask(with: url,completionHandler: { (data, response, error) in
            if data != nil{
                let image = UIImage(data:data!)
                if(image != nil){
                    DispatchQueue.main.async(execute: {imageView.image=image
                        
                    })
                }
            }
        })
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
